# LIS4368-A2 
## Jessica Motlow

### Assignment 2 Requirements:

*Assessment links:*

1. [http://localhost:9999/hello](http://localhost:9999/hello)
2. [http://localhost:9999/hello/index.html](http://localhost:9999/hello/index.html)
3. [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)
4. [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)
5. [http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi)


#### Screenshot:

*Screenshot of query results for a2*: 

![queryresults.png](https://bitbucket.org/repo/XokE4B/images/220683746-queryresults.png)